<div class="col-sm-9">
    <div class="blog-post-area">
        <h2 class="title text-center">Latest From our Blog</h2>

        <?php if (count($news)) {
            foreach ($news as $value) { ?>

        <div class="single-blog-post">
            <h3><?= $value->getHeader(); ?></h3>

            <div class="post-meta">
                <ul>
                    <li><i class="fa fa-user"></i> <?= $value->getAuthor(); ?></li>
                    <li><i class="fa fa-clock-o"></i> <?= $value->getCreatedAt(); ?></li>

                </ul>
								<span>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star-half-o"></i>
								</span>
            </div>
            <?php $url1 = Framework\Templating\ViewHelper::url('news.item', array('id' => $value->getId())); ?>
            <a href="<?= $url1; ?>">
                <img src="<?= $value->getPicture(); ?>" alt="">
            </a>

            <p><?= $value->getShortNew(); ?></p>
            <a class="btn btn-primary" href="<?= $url1; ?>">Read More</a>
        </div>
        <?php }} ?>
        <div>
            <?php
           if ($numPages > 1) {
                echo "<ul class='pagination'>";
                for ($i = 1; $i <= $numPages; $i++) {
                    $url = Framework\Templating\ViewHelper::url('news.index', array('page' => $i));
                    if ($i == $currentPage) {
                        $class = 'active';
                    } else {
                        $class = '';
                    }
                    echo  "<li class='".$class."'><a href='".$url."'>".$i."</a></li>";
                }
                echo "</ul>";
            }
            ?>
        </div>
    </div>
</div>

