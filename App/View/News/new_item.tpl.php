<div class="col-sm-9">
    <div class="blog-post-area">

        <h2 class="title text-center">Latest From our Blog</h2>
        <div class="single-blog-post">
            <h3><?= $newsItem->getHeader(); ?></h3>
            <div class="post-meta">
                <ul>
                    <li><i class="fa fa-user"></i><?= $newsItem->getAuthor(); ?></li>
                    <li><i class="fa fa-clock-o"></i> <?= $newsItem->getCreatedAt(); ?></li>

                </ul>
								<span>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star-half-o"></i>
								</span>
            </div>
            <a href="">
                <img src="<?= $newsItem->getPicture(); ?>" alt="">
            </a>
            <p>
                <?= $newsItem->getFullNew(); ?> </p>

        </div>
    </div><!--/blog-post-area-->

    <div class="rating-area">
        <ul class="ratings">
            <li class="rate-this">Rate this item:</li>
            <li>
                <i class="fa fa-star color"></i>
                <i class="fa fa-star color"></i>
                <i class="fa fa-star color"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
            </li>
            <li class="color">(6 votes)</li>
        </ul>

    </div><!--/rating-area-->

    <div class="socials-share">
        <?php include(__DIR__.'/../social_icons.tpl.php'); ?>
    </div><!--/socials-share-->

</div>