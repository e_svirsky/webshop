<?php

namespace App\Entity;

use Framework\Database\Repository;
/**
 * Created by PhpStorm.
 * User: NotePad
 * Date: 18.08.2015
 * Time: 15:45
 */
class OrderDetails extends Repository
{
    public static $staticTableName = 'ws_order_details';

    private $id;

    private $orderId;

    private $goodsId;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param mixed $orderId
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @return mixed
     */
    public function getGoodsId()
    {
        return $this->goodsId;
    }

    /**
     * @param mixed $goodsId
     */
    public function setGoodsId($goodsId)
    {
        $this->goodsId = $goodsId;
    }

}