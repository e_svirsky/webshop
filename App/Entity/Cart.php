<?php

namespace App\Entity;

use Framework\Database\Repository;
/**
 * Created by PhpStorm.
 * User: NotePad
 * Date: 18.08.2015
 * Time: 15:44
 */
class Cart extends Repository
{
    public static $staticTableName = 'ws_cart';

    private $id;

    private $goodsId;

    private $userId;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getGoodsId()
    {
        return $this->goodsId;
    }

    /**
     * @param mixed $goodsId
     */
    public function setGoodsId($goodsId)
    {
        $this->goodsId = $goodsId;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param mixed $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }
}
