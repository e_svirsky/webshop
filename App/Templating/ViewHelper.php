<?php

namespace App\Templating;

use App\Entity\Category;
use App\Entity\Good;
use Framework\Http\Request;
use Framework\Http\Routing;

class ViewHelper
{
    public static function getCategoriesInfo()
    {
        return Category::getCategoriesInfo();
    }

    public static function getCurrentUrl()
    {
        return $_SERVER['REQUEST_URI'];
    }

    public static function isGoodOfCategory($categoryId)
    {
        if (Routing::$currentRouteName == 'shop.good') {
            $requestParams = Request::getRequestParams();

            $goodId = $requestParams['get']['id'];
            $good = Good::findOneBy(array('id' => $goodId));
            if ($categoryId == $good->getCategoryId()) {
                return true;
            }
        }

        return false;
    }

    public static function isNewItem()
    {
        if (Routing::$currentRouteName == 'news.item') {
            return true;
        }

        return false;
    }
}