CREATE TABLE IF NOT EXISTS `ws_news` (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `header` varchar(255) NOT NULL,
	  `picture` varchar(255) NOT NULL,
	  `short_new` text,
	  `author` varchar(255) NOT NULL,
	  `created_at` DATETIME NOT NULL,
	  `full_new` text,
	  PRIMARY KEY (`id`)
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

	INSERT INTO `ws_news` (`header`, `picture`,`short_new`, `author`, `created_at`, `full_new`) VALUES
	("Life in Hard Mode: Fashion Week and Anorexia",
	'/images/blog/blog-one.jpg',
	"See what’s new in at www.veromoda.com today and be the first to shop the hottest products. Hundreds of never before seen dresses, tops, shirts, tunics, coats, jackets, knits, skirts, trousers, jeans and accessories are ready for you to shop, ahead of the crowd. The new in edit also includes styles from our popular sub-brand noisy may. Check in to see our new arrivals every day!",
	'Jon Carter',
	'2015-07-06 17:37:10',
	"I am excited, and I am terrified.
Fashion Week: for a young woman recovering from anorexia, attending fashion week, surrounded by beautiful people, skinny models, and a society which is centered around appearances, is similar to sending a recovering alcoholic to a bar that serves free drinks all night. Basically, a huge trigger.
So why have I decided to attend? Because I want to challenge myself. It might seem masochistic of me to do this so early on in my recovery (a year now with ups and downs) from this disease. I seem to be playing the game of life and continuously and intentionally choosing the 'Hard Mode' even though the 'Easy Mode' might be a better option for me. But we all know that the hard mode is so much more fun, and for for anybody who knows me, I cannot turn down a challenge. I no longer want to think of myself as a 'sick' person who cannot explore and experience the world because it is filled with dangerous messages which might offset my recovery. I want to think of my recovery in a different way - recovery as a perilous and exciting journey which is a testament to my strength.
How many times can the eating disorder yell and scream at me with its messages of low-carb diets and 'bikini-ready' workout plans, juice fasts and cleanses, magazine articles with waif-like actresses and models wearing the hottest trends - but then I yell back even louder: 'I DON''T CARE! I love myself and this is all a bunch of B.S.!' Then I head to dinner with friends who are supportive, loving, and understanding and enjoy a large bowl of pasta without counting one single calorie. That is recovery on steroids.
Life will continue to present me with these triggers, and I can either let myself hide in a corner, or I can face them head on, fight back with my own truth and even more - I can spread this truth to others. So to the hard mode in this game of Life - bring it on."),

	('Get Your Givenchy New York Fashion Week Tickets Now',
	'/images/blog/blog-two.jpg',
	"Givenchy''s spring 2016 men''s wear collection was presented in Paris this summer, but the women''s show is coming to New York.",
	'Valerio Mezzanotti',
	'2015-09-02 11:30:30',
	"In what is the first example of a brand opening its fashion week show to the wider public that I can remember — and the top of a potential slippery slope of fashion week transformation — at 10 a.m. on Wednesday, Givenchy will offer 820 seats to its spring 2016 extravaganza to, well, whoever gets there first. It’s certainly a first for Givenchy.
O.M.G.! O.M.G.! And all that.
It’s a smart move. The brand will be showing in New York, as opposed to the usual Paris, to mark the opening of its giant new Manhattan flagship (having a show to open a store is something of a new thing among designers: Valentino moved its couture show to Rome from Paris in July, for its new flagship there). This brings the city and its shoppers, literally, in.
Even more important, however, the Givenchy giveaway could ameliorate some of the ickiness around the fact that the show will be held on Sept. 11. There’s no getting away from the weirdness and discomfort of celebrating a temple to consumerism on a day of memory and mourning, but putting the event in the context of celebrating the resilience of a city and its people, with a certain openness as opposed to exclusion, will probably help.
In any case, I fully expect the website to “experience difficulties due to demand” when applications go live — given the history of such limited fashion offerings as Lily Pulitzer for Target or Alexander Wang for H&M. Not that they really are comparable, being collections as opposed to tickets.
But there’s a very good chance, if the response is as crazed as anticipated, that this will set off a new trend, one even more influential than the Givenchy designer Riccardo Tisci’s luxury sweatshirts. Other brands will undoubtedly be watching closely.
Live stream, after all, may be great. But it doesn’t come close to live."),

	("Bradley Soileau Doesn’t Care What You Think of His Clothes",
	'/images/blog/blog-three.jpg',
	"Kelly is wearing the Slug Trench, made of black waxed Scottish canvas with 'hurt so good' embroidered on the back, $1,400; For information: blackfistlabel.com; Blackfist x The/End ''Jodie''s Jeans', made of hand-distressed, repaired, washed, washed, and oil-washed indigo denim with 'hurt so good' embroidery on left thigh, $995.",
	'Matthew Foley',
	'2015-09-03 12:38:50',
	"I met Bradley Soileau in the back of a smoky bar in Hollywood. I was in Los Angeles for work, and had temporarily fled the siren call of room service and movies on demand for a long overdue reunion with Smiley Stevens and Philippa Price, a pair of New York City–escapees who had moved to the West Coast to start their own streetwear label, Guns Germs $teal. Sitting in the beer-sticky booth in my favorite oversized black sleeveless 6397 denim jumpsuit, I couldn’t help but feel both impressed and intimidated by Philippa’s boyfriend, Bradley, who possessed model good looks, a forehead tattoo which read “war inside my head,” and a super-dope and innately authentic skater-meets-surfer-meets-street-urchin style that was essentially comprised of all the looks that I try to channel on a daily basis. I was sold, and, admittedly, quite chuffed that my new aesthetic inspiration was human, rather than a collection of vintage photographs of Kurt Cobain and Jimi Hendrix, that he had worked as the creative director of FourTwoFour on Fairfax (possibly one of the coolest stores in L.A.), and happened to have matching rooty, shoulder-length blonde hair. Plus, he liked my jumpsuit.
But it was the next time I saw him—at a party clad in the most supreme floral brocade coat, which, after pestering his girlfriend, I found out he had designed—that I knew we needed to seriously talk fashion. And it turns out it was just in time: Bradley, who is a self-described “skater/metalhead turned metrosexual punk kid turned emo kid turned hardline straight edge hardcore bro into what I am now,” is hard at work on his fledgling men’s line, Blackfist, now on its second collection entitled “Hurt So Good.” With a denim collaboration with The/End under his belt and fans like Je$$e Rutherford, Lil Wayne, Mod Sun, and Ty Dolla $ign already snapping up the wares, it’s safe to say he’s just getting started—and I feel one step closer to finding clothes that don’t follow any rules, clothes that feel like an extension of me. Here, the designer goes off the cuff and on the record about his inspirations, and why he doesn’t really care about what anybody else is doing.
Tell me about Hurt So Good.
It’s my take on the eighties thrash skate scene, with inspiration from the Troma film The Toxic Avenger, who was a little nerdy guy who [falls into a vat of toxic waste and] eventually goes on to fight evil. Everyone is scared of him because he’s a gross mutant freak, but he falls in love with a blind girl and saves Tromaville. I like to use that as an analogy of me and my life. I’m just a guy from Louisiana, no one knew me, no one cared, I moved to NYC and got scouted to be a model by my agent Dave Fothergill at Red models. Two months later, I was cast for the Lana Del Rey video “Born to Die,” and suddenly I kind of blew up, I started working a lot more, people started stopping me to take pictures and I went from no one knowing me to a lot of people knowing me and not really caring. We both had to go through something large to get somewhere “great,” but once we were there, it wasn’t as great as it should be. We both became heroes of a sort, but no one wants to be friends with heroes and that hurts. That’s why it’s “Hurt So Good.”
How do you describe the Blackfist aesthetic? What was your motivation behind launching it?
It’s rebellion, sex, violence, aggression. It’s good and bad and sexy and ugly. It’s for someone who wakes up every day and says, I want to be loud and angry, or, I want to get fucked, or, I don’t really give a shit what I look like but I still look awesome. That’s the aesthetic, that’s the person who should want to wear it and who would vibe with it or understand it. It’s for the throwaways with style, the forgotten ones that look like shit but dress better than your latest fashion icons. I started it because I wanted to show people that there’s still art in fashion, it all hasn’t just become a consumer-driven business . . . to make art for myself. To leave a lasting impression on the world so when I die it wouldn’t have been for nothing.
What makes it different than other brands?
It’s real. It’s really what I would wear any day of the week. It’s really how I feel about the world. It’s really music I’ve listened to, things I’ve seen, places I’ve been to, and feelings that I’ve had. It’s an extension of me. And that’s a big difference because a lot of brands are just aesthetics. And also, I don’t follow rules. I don’t care enough to. This is supposed to be fun for me and I’m running my own business so I don’t have to follow rules. Seasons shmeasons. I’m gonna make what I want, when I want, and I don’t really care what anyone else has to say or do about it. That’s what separates me from most."),

	('18 Models Who Could Be the Next Gigi Hadid',
	'/images/blog/blog-four.jpg',
	"It was Gigi Hadid who stood out among the pack since rising to superstardom at last season''s Fashion Week. But who will score the coveted spot this year? Is there someone who''s a must watch, ready to claim her crown as It girl as she struts down the catwalk?",
	'Samantha Sutton',
	'2015-09-02 09:15:00',
	"It was Gigi Hadid who stood out among the pack since rising to superstardom at last season's Fashion Week. But who will score the coveted spot this year? Is there someone who's a must watch, ready to claim her crown as It girl as she struts down the catwalk?
Well, we can't predict the future, but there are a handful of faces we're excited to see do their thing. Scroll through for 18 women we think have potential to really make it big — big big — as we prep for the most exciting time of year.
");

