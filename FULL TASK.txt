------------------------------
ЭТОТ ФАЙЛ НЕ В КОЕМ СЛУЧАЕ НЕ МЕНЯТЬ!!!
------------------------------

Значит в этой папке лежит полный HTML шаблон со всеми необходимыми страницами.
Твоя задача - на основе этого шаблона сделать работоспособный интернет магазин.

1. Начнём с базы данных.
Схема (структура) базы такая:

ws_user (пользователь который закажет товар)
------------------------------
id -> уникальный ID таблицы
session_id -> string(255)
first_name -> string(255)
last_name -> string(255)
phone -> string(255)

ws_category (категория товара)
------------------------------
id -> уникальный ID таблицы
name -> string(255)
description-> text

ws_good (собственно товар)
------------------------------
id -> уникальный ID таблицы
category_id -> integer(10)
name -> string(255)
description -> text
price -> string(255)
picture -> string(255)

ws_cart (Корзина товаров)
------------------------------
id -> уникальный ID таблицы
good_id -> integer(10)
user_id -> integer(10)


ws_order (заказанные товары)
------------------------------
id -> уникальный ID таблицы
user_id -> integer(10)
description -> text
phone -> string(255)
delivery_type -> string(255)
total_sum -> string(255)

ws_order_details (товары в заказе)
------------------------------
id -> уникальный ID таблицы
order_id -> integer(10)
good_id -> integer(10)


------------------------------
Необходимо написать код SQL для этих таблиц и сохранить его в файл db/database.sql. Прямо можешь там и работать.

2. Создание структуры проекта в PHP:
Ознакомься со структурой папок. Будем строить MVC модель для нашего приложения.
В папке classes - будут лежать вспомогательные классы и контроллеры, в папке views - наши вьюшки.
Задание:
Привяжи в OpenServer домен в папку web, затем тебе нужно создать файл .htaccess и сделать так, чтобы была только единственная точка входа в приложение - это файл index.php в папке web.
Про точку входа - почитай тут: http://habrahabr.ru/post/115389/

После того как ты сделала точку входа - сделай автозагрузку файлов классов в index.php.
Создай в папке classes - файл FrontendController.php
Этот класс будет отвечать за парсинг роутинга и рендеринг шаблонов и логики.
В index.php - добавь создание экземпляра класса FrontendController, затем сделай вызов метода parseRouting у экземпляра.
Создай этот метод в FrontendController.
Попытайся написать роутинг для приложения.
Принцип работы его: В зависимости от get-запроса, твоя функция должна вызывать тот или иной контроллер и метод контроллера.
Для этого создай контроллер ShopController и там метод indexAction.
И попытайся сделать так, чтобы при запросе в строке браузера /index - вызывался метод indexAction.
Для проверки - также создай роутинг для строки /category?id=1 - и вызываться должен метод categoryAction.

3. После того, как роутинг готов - пора бы написать класс который будет рендерить view-шки.
Для этого создай класс View в нашей папке классов и создай у класса метод renderView:

function renderView($file, $vars=null)
{
    if (is_array($vars) && !empty($vars)) {
        extract($vars);
    }
    ob_start();
    include $file;

    return ob_get_clean();
}

Почитай в гугле про эти методы:
extract
ob_start
ob_get_clean

Ты должна понять как работает этот метод.

Теперь сделай тестовый файл в папке views - название index.tpl.php
И в тестовом контроллере ShopController::indexAction - воспользуйся методом парсинга
Т.е. создай экземпляр класса View и вызови метод, и посмотри - отработает ли шаблон.
Попробуй передавать туда переменные - и вызывать их в этом файле.
УБЕДИСЬ, что всё работает как задумано.

4. Оптимизация работы MVC фреймворка.
Всё бы хорошо - но у вас есть лишние вызовы которые не хотелось бы иметь в контроллере. Например создание экземпляра класса View.
Чтобы этого избежать - создай абстрактный класс AbstractController и в нём создай метод render c такими же входными параметрами как и в renderView.
Теперь наследуй все наши контроллеры от AbstractController и попробуй вызвать метод render из класса контроллера.
Убедись что всё работает как и должно. Проверь - отправляются ли переменные в шаблон - вызови их, протестируй всё тщательно.

5. Следующий шаг - давай разделим наши вьюшки на папки. Т.е. для Shop контроллера - будет папка views/Shop и там уже будет index.tpl.php и так далее...
Для Admin контроллера - сделай папку views/Admin.
Еще один момент, у каждого метода контроллера должен быть основной шаблон, чтобы нам его не переопределять в каждом шаблоне.
Поэтому создаём в классе контроллера свойство класса с именем $templateName и присваиваем ему путь к основному шаблону:
$templateName = 'views/Shop/layout.tpl.php';
Создай этот файл и напиши в нём псевдо-переменную %content%. В эту переменную мы будем вставлять результат парсинга нашего основного шаблона.
Т.е. будет шаблон layout и в нём внутри будет уже парсится данные с шаблона который мы отрендерим в контроллере. Т.е. своеобразная иерархия вложенности шаблонов.
Теперь в методе renderView - нужно это всё описать.
Меняем наш метод renderView - добавляем еще один входной параметр. Итого сейчас будет 3 параметра:
function renderView($layoutFile, $file, $vars=null)
И внутри мы должны сделать так чтобы отрендерился сначала один шаблон - а потом подставить туда результат.

function renderView($layoutFile, $file, $vars=null) {
    ob_start();
    include $layoutFile;

    $layoutContent = ob_get_clean();

    if (is_array($vars) && !empty($vars)) {
        extract($vars);
    }
    ob_start();
    include $file;

    $content =  ob_get_clean();

    return str_replace('%content%', $content, $layoutContent);
}

В этой функции - мы сначала рендерим 1й шаблон - основной, потом второй - шаблон метода контроллера и просто вставляем один в другой.
Разберись как работает эта функция.
Протестируй что всё работает как должно.
Таким образом в каждом контроллере - будет всегда основной шаблон, мы можем его переопределить или в принципе опустить.

Кстати в абстрактном классе, где идёт вызов метода renderView нужно подставить еще переменные. Не забудь это сделать.

6. Следующий большой этап - это работа с базой данных.
Создай папку models - там будут лежать PHP файлы наших моделей (классов таблиц баз данных).
В этой папке для каждой таблицы из нашей БД (смотри в файле db/database.sql - нужно создать класс. Класс будет называться по имени таблицы БЕЗ префикса ws_.
т.е. ws_user = User и так далее.
У каждого класса создай свойства класса соответствующие названиям полей таблицы, т.е. для класса User это будут свойства:
$id, $sessionId, $firstName, $lastName, $phone
Эти свойства должны быть с модификатором доступа private.
Теперь пишем абстрактный класс модели в котором будет логика работы с базой данных.
В этой же папке создай абстрактный класс Model, пока что пустой и наследуй все свои классы моделей - от этого класса.

7. Пишем метод find.
В классе Model создай публичный метод find.
Этот метод будет записывать в наши свойства класса - актуальные данные поля в таблице.
На входе у метода будет параметр $id - уникальный идентификатор таблицы.

Теперь напишем промежуточный класс DatabaseConnection - создай его в этой папке.
У этого класса создай статический публичный метод getInstance используя паттер Singleton. Ты его проходила - поэтому у тебя не должно составить это труда, иначе используй гугл.
Тебе нужно создать подключение к базе данных используя этот паттерн. Можешь поискать примеры в интернете, можешь сама попробовать написать.
Все параметры подключения - логин пароль и название базы данных - помести их в константы класса
На выходе должен получиться объект PDO для работы с БД.
Не забудь заглушку поставить для конструктора - чтобы никто не смог вызвать создание объекта через new.
Теперь когда это готово - сделай метод в этом классе query - он будет исполнять запрос который будет подаваться на входе.
в методе query - вызови метод PDO - query и туда подставь входящий параметр $query.
Обработай результат полученный и верни функцией - массив данных. Ты тоже знаешь как это делается, если забыла - используй гугл.
Теперь когда у нас есть соединение с базой данных - допиши функцию find:
Для начала в каждый из классов добавь свойство c модификатором доступа protected с именем $tableName = 'ws_order'... и так далее.
Напиши запрос, используя это свойство (для указания названия таблицы), создай подключение к БД и выполни его и верни результат - массив с данными.

7.1 Сейчас метод find - выводит массив из базы данных. Сделай этот метод статическим. Далее - переименуй класс Model в Repository. Соответсвенно везде где мы его наследовали - тоже переименуй.
Теперь - нам нужно сделать, чтобы метод find возвращал не массив а объект соответсвующего класса. К примеру мы вызываем User::find(1) - возвращается экземпляр класса User где все свойства его сооветствующие равны значениям в базе данных.
К примеру свойство $sessionId = session_id из базы данных. Т.е. В методе find - нужно создать экземпляр вызываемого класса и за set-ить ему все его значения... setSessionId ... и так далее.
Если такого нет объекта в базе данных (с запрошенным id) - должен вернуться null.

7.2 Создай в классе Repository свойство с названием $isNew, с модификатором доступа private. Создай также конструктор к этому классу - и при создании объекта в конструкторе - по умолчанию присваивай этому свойству значение true.
Но сделай возможность чтобы при создании объекта можно было изменить свойство на false. Т.е. в конструкторе должен быть не обязательный параметр, который будет выполнять описанные функции.
И в методе find, где ты будешь создавать экземпляр класса - при создании экземпляра указывай false. Это свойство будет указывать - объект создан новый или этот объект уже взят из базы данных...

8. Пишем метод save.
Для написания этого метода нам придется воспользоваться классом Reflection из php. Почитай про него тут:
http://php.net/manual/ru/book.reflection.php
Задача - с помощью этого класса получить все свойства с модификатором PRIVATE родителя (т.е. к примеру класса User). это нужно для того чтобы сохранить эти данные в базу данных.
Схема такая:
находим все свойства, пробегаемся foreach-ем по ним, составляем запрос типо:
INSERT INTO 'table_name' (field_1, field_2, ...) VALUES (value_1, value_2, ...);
  либо
UPDATE 'table_name' - если этот объект существует уже.
затем выполняем запрос с помощью метода query - написанного ранее.

9. Добавляем статический метод findOneBy($params) - здесь $params - массив с параметрами. Тоже самое - генерируем SQL и возвращаем (если найдены в базе записи) - один объект.

10. Добавляем статический метод findBy($params) - тоже самое что и findOneBy, но тут уже будет результат - массив объектов соответствующих.

10.1 У обоих методов 9 и 10 - сделать проверку на не сущствование введенного параметра, используя рефлекшин.

11. Пишем публичный метод remove. Применяется он к объекту, что делает - понятно. Удаляется текущий объект. Используй SQL запрос DELETE FROM ... WHERE id='...'.
Добавь в класс Repository - приватное свойство $removed - и проверяй его перед удалением. И если объект удалён уже - выкидывай ошибку.
Т.е. свойство $removed по умолчанию будет равно false. Если на объект вызван метод remove - то свойство делаем true. Перед удалением всегда проверяем - если это свойство уже true - значит ничего не удаляем и кидаем ошибку, что "Объект уже удалён!".
Также напомни мне чтобы я проверил с этим свойством также свойство $isNew.

В методе findBy - могут задаваться параметры limit и offset в массиве. Нужно сделать проверку, и если они есть - в SQL добавить соответсвенно LIMIT или OFFSET на указанное значение.
Почитай что делают эти операторы в MYSQL - разберись как они работают и добавь этот функционал.

Пока с базой данных - всё. Если нам понадобятся еще какие методы - допишем их в процессе.
Потестируй работоспособность наших методов создания и удаления из БД.

Твои классы моделей должны уметь - создаваться, редактироваться и удаляться. Проверь что всё это работает - поздавай в методе контроллера всякие разные варианты создания объектов и убедись что всё работает.

Для этого в методе нашего контроллера - создай объект класса, к примеру User, задай ему значения и сохрани save().
Проверь наличие появившегося в базе объекта. Попробуй редактировать его. Сделай выборку всех объектов и так далее.
В общем протестируй работоспособность всей системы.

12. Следующее задание - модифицируем наш FrontendController. Дело в том, что у нас сейчас собираются только $_GET-параметры запроса, но бывают ведь еще и ... какие? - правильно $_POST.
Чтобы напрямую не обращаться к суперглобальной переменной $_POST - это очень плохо то, мы сделаем обёртку для этого.
Меняем функционал FrontendController-а - теперь он будет возвращать в метод контроллера не $newParams а объект класса Request. Для этого создадим в classes файл.php и объявим там класс Request.
У этого класса будет 2 свойства приватных $postParameters и $getParameters. Создай для них сеттеры и геттеры. Также будет приватное свойство $isPost. В него мы будем записывать true - если пришёл POST запрос с сервера и false - в остальных случаях.
Для этого свойства - создай метод публичный isPost - который будет возвращать это свойство, а также публичный сеттер - setIsPost который будет это свойство изменять.
Теперь модифицируй метод parseRouting в FrontendController, чтобы он отдавал объект класса Request в метод. Перед этим присвой его свойства нужные значения - получи POST, GET - у тебя уже получен и установи свойство isPost.
Измени метод indexAction - поставь на вход $request и проверь - всё ли правильно работает.

13. Меняем таблицу роутинга чтобы можно было генерить роуты для приложения.
Иногда во view нам придётся генерить роуты, для этого нам нужно будет изменить таблицу роутов.
Итак - создаём класс Configuration.php в classes. Там будут хранится параметры всех нашим настроек.
В классе - создаём статический метод routeTable и туда перемещаем всю нашу таблицу роутов из конструктора FrontendController-а. Не забудь return. А в конструкторе FrontendController-а вызываем Configuration::routeTable().
Также создай статический метод databaseParams и туда помести все настройки базы данных из класса DatabaseConnection. а в классе DatabaseConnection - вызови этот статический метод и получи все настройки, константы удали из класса.
Понимаешь что мы делаем? Мы все настройки приложения перемещаем в одно место.
Теперь смотри - мы добавим уникальное имя для каждого нашего роута, чтобы можно было сгенерировать роут по названию в шаблоне (view). Для этого - меняем структуру массива таблицы роутов:
у нас теперь будет так:
<название роута> => array(
  <путь роута>,
  <название контроллера>,
  <название метода>,
  array(<массив необходимых параметров>)
)

Вот пример для роута категорий:
'shop.category' => array(
  '/category',
  'shop',
  'category',
  array('id')
)

Теперь поправь метод parseRouting - он сейчас не будет работать. Сделай чтобы работал как и раньше.
Так как нужно будет еще работа с роутами - пора завести такой класс.
Создаём класс Routing в classes.
Создай там публичный статический метод parse и перемести туда функционал с parseRouting. Теперь в index.php в папке web - вызови этот статичесий метод Routing::parse() вместо создания класса FrontendController-а и вызова метода parseRouting.
Теперь класс FrontendController можно удалить.

После такого рефакторинга - проверь работоспособность нашего фреймворка. Попробуй запросить разные урлы и посмотреть рендерится ли шаблоны и так далее...

14. Создание метода для генерации ссылки роута.
В классе Routing - создаём статический публичный метод generateUrl с параметрами ($routeName, $routeParams = array()).
Итак, чтобы сгенерировать роут - нужно найти его в таблице роутов: для этого получим таблицу через класс Configuration.
К примеру если мы задаем $routeName = 'shop.category' с параметрами array('id' = 5)
На выходе должно получиться что-то типо: '/category?id=5'.
Чтобы это проверить вставим эту реализацию в абстрактный класс AbstractController. Для этого создадим там публичный метод generateUrl с такими же параметрами как и в схожем методе класса Routing и вызовем статический из класса Routing, передав необходимые параметры.
Для проверки - в методе контроллера попробуй вызвать $this->generateUrl('shop.category', array('id' => 5)) и выведи результат.

14.1 Переименуй метод generateUrl в AbstractController в redirectTo. Будем перенаправлять пользователя в этом методе. Переделай функционал, чтобы происходил редирект на полученный роут.
Проверь работоспособность нового метода.

15. Пишем класс хелпера для view.
Иногда во view нужно будет вызывать полезные функции, поэтому создаём класс ViewHelper в папке classes.
Там создадим метод статический url() с параметрами как в generateUrl класса Routing.
Эта функция будет делать тоже самое, только для view.
Также создай алиас - псевдоним для класса ViewHelper. Пускай это будет vh. Это нужно для того, чтобы не захламлять код нашего view -
вместо вызова ViewHelper::url будет vh::url.

16. Итак, мы делали до этого проект без пространств имён - но на дворе - 2015й и нужно пользоваться современными технологиями.
Давай-ка переведём проект на namespace-ы. Для начала почитай что это такое и для чего:
http://php.net/manual/ru/language.namespaces.php
и еще вот это:
http://habrahabr.ru/post/132736/
Теперь когда более менее понятно, с чем мы имеем дело, начнём.
Создай папку в нашем проекте App. Внутри неё папки:
Config, Controller, Entity, Migration, View.
Теперь переносим - класс Configuration в папку Config
Все пользовательские контроллеры (Shop, Admin) переносим в папку Controller.
Все объекты базы данных (их 6) переносим в папку Entity.
файл db/database.sql переносим в папку Migration.
всё из views в папку App/View

С этим завершили. Теперь создай папку Framework.
Внутри неё папки:
Controller, Database, Templating, Http
И теперь переноси:
оставшиеся контроллеры (абстрактный) перенеси в папку Controller.
Репозиторий и DatabaseConnection в Database.
фалй View перенеси в папку Templating,
и файл Routing и Request перенеси в папку Http.
Теперь у нас чётко разделено - где файлы пользовательские а где файлы фреймворка.
Теперь нужно в каждом классе правильно задать пространство имени.
Для этого в каждом файле класса нужно добавить после объявления <?php - отступить одну пустую строку и написать:
namespace App\Controller; - для классов лежащих в соответствующей папке. И так для каждого файла.
Теперь пройдись по всем файлам и проверь - не показала ли ошибок IDE-шка. Если да - подтяни нужные прстранства имён с помощью use ...;
И самое последнее - измени автолоадер:

function __autoload($class) {
    $class = '../'.str_replace('\\', '/', $class) . '.php';
    require_once($class);
}

Теперь у нас будут подтягиваться любые классы из папок и папки, если мы будем корректно использовать namespace.

17. Сделаем еще одну правочку. У нас сейчас в AbstractController - неверно задано имя шаблона. Точнее не универсально.
Только для Shop папки, а если создадим другой контроллер AdminController - будет всё та же папка шоп, а должна быть Admin.
Итак - убираем по умолчанию значение у этой переменной и создаём конструктор.
В конструкторе - присвоим ей правильное значение:
Получи имя вызываемого класса в конструкторе - обрежь его грамотно и добавь в путь в соответствующем месте.
Теперь имя будет правильно задаваться. И шаблоны будут разные. Провреь это - Для контроллера Admin создай в папке views папку Admin - помести туда файлик нужный.
И проверь что он вызывается.

18. Делаем функционал хелперов для шаблонов в нашем фреймворке.
/* Написать задачу когда будет произведён рефакторинг с неймспейсами */

19. Начинаем натягивать шаблон сайта на фреймворк.
Итак - удалим пока что из таблицы роутов все левые роуты, оставим только один - index.
И из нашей директории App\Controller - удалим все контроллеры кроме одного - ShopController.
Начинать работу будем с ним.
Удалим все лишние методы в этом контроллере - оставим только один - indexAction.
Теперь скопируй в файл шаблона для этого контроллера Shop/layout.tpl.php всё из файла index.html папки html template.
Этот шаблон еще будет использоваться в других экшинах - поэтому поставь в нужном месте псевдопеременную %content% и вырежи всё что перенесём в темплейт метода index.tpl.php контроллера.
Вырезать из основного шаблона нужно то что будет меняться в кажом экшине. В данном случае - это список товаров рандомных на этой странице.
Теперь когда это сделано - начинаем вселять жизнь в мёртвые шаблоны html)
Для начала давай заполним наши пункты меню.
Home - для этого пункта у нас есть уже роут
Categories - для этого пункта создай метод categoryAction, и роут для этого метода c именем app.shop.category
Cart - для этого пункта создай метод cartAction и соответствующий роут.
News - для этого пункта - создай контроллер NewsController и метод indexAction и соответствующий роут
Feedback - для этого пункта - создай контроллер FeedbackController и метод indexAction и соответствующий роут
My Orders - для этого пункта - создай метод ordersAction в контроллере Shop и соответствующий роут
Теперь вызови в шаблоне генератор url-ов и сделай ссылки для этих пунктов меню. Снизу в footer-е - тоже добавь эти ссылки.
Теперь нам нужно отрисовать список категорий.
Создай в базе данных категории указанные в шаблоне. Каждой категории создай тестовое описание. Всё на англ - сайт будет для америкосов.
Лучше всего напиши INSERT-ы для этих категорий тестовых чтобы в случае чего - мы могли их загрузить в базу данных.
Эти INSERT-ы запиши в файл categories.sql и в папку наших sql файлов сохрани.

20. Создай тестовые товары в таблице товаров таким образом, чтобы в каждой категории было по 3-4 товара. Запиши этот SQL запрос в файл и сохрани в нашей папке Migrations.

21. На главной странице отображаются сейчас товары в самом шаблоне. Сделай так, чтобы товары эти были реальные. В методе контроллера получи произвольных 6 товаров и выведи их на главной странице.
Ссылки на товар мы сейчас не можем сгенерить - поэтому давай сделам новый экшин в контроллере Shop, назови его goodAction. Это будет экшин для карточки одного товара. Добавь такой роут, пускай url будет /good,
не забудь добавить обязательный параметр id.
Мы сделали роут для товаров - поэтому ссылки мы сейчас тоже можем сделать для товаров на главной странице.
Ссылку сделай там где название товара. Сейчас там нет ссылки - добавь её (элемент a) и туда вставь в аттрибут href - наши ссылки на товары. Проверь что всё работает.

22. Измени главный роут shop.index чтобы запрос был не /index, а /. Чтобы главная страница грузилась при запросе webstore.lb. Так будет логичнее.

23. Делаем следующую страницу - страницу категории. Эта страница будет похожая на главную, за тем лишь исключением, что она будет отображать товары определенной категории.
В нашем методе - выбери все товары выбранной категории и помести их в шаблон. В шаблоне - отобрази.

24. Далее - информация по одному товару. Всё тоже самое - в нашем роуте goodAction - получи информацию по выбранному товару, и передай её в шаблон, после этого - отобрази все данные в шаблоне.
Образцом для этой страницы послужит файл "html template/product-details.html". Блок с отзывами и рейтингами мы убираем. А вот рекоммендованные товары - оставим. Отображай здесь 3 любых товара из текущей категории.
Вместо вот этих характеристик:

Availability: In Stock
Condition: New
Brand: E-SHOPPER

Отобрази категорию:
Category: <название категории>

Также отобрази кнопки соц сетей в карточке товара.
Для этого воспользуйся сервисом:
http://share.pluso.ru/

25. Делаем функционал для добавления товара в корзину. Поупражняемся в ajax.
Итак - создай метод в контроллере Shop, который будет принимать запрос на добавление. Пускай url будет /ajax/add-to-cart?id=<id> к примеру.
Т.е. обязательный параметр будет id.
Для начала - сделай чтобы там, где рендериться один товар в списке - в элементе кнопки добавлялся атрибут data-id и туда клади значение id товара.
Так мы будем на сервер слать - какой товар хотят отправить в корзину.
Создаём файл custom.js в папке web/js. Туда будем писать обработчик для нашей кнопки.
Добавь этот файл в head в основном шаблоне. Проверь что он добавился, сделай метод в файле, который отработает по завершению загрузки страницы:
$(function() {

});
И внутри него для начала запиши alert('Я загрузился');
Теперь когда всё работает - добавляем селектор для кнопок корзины и пишем обработчик на клик мышкой. Почитай как это сделать в гугле.
Ты должна отправить ajax запрос на сервер на урл /ajax/add-to-cart?id=<id>. Взять из аттрибута который мы создали data-id и отправить его на сервер.
На сервере получи id-шник товара и запиши этот товар в таблицу корзины для этого пользователя.
В файле web/index.php - активируй сессии, погугли как, если не помнишь.
В методе контроллера который обработает ajax - верни ответ: status => 1.
В js обработчике - если пришёл такой статус - вызови модальное окно bootstrap и напиши туда, что товар успешно добавлен (на английском конечно).

26. Добавь CONSTRAINTS для всех внешних ключей в базе данных. Создай еще один файл в папке Migration и напиши соответствующий код.

27. Мы уже сделали обработчик для добавления товаров в корзину, теперь давай сделаем страницу корзины. Шаблон корзины возьми из файла "html template/cart.html".
Обрати внимание что тут будет другой layout для этой страницы - не такой как во всех других страницах.
В левом нижнем блоке всё убери и добавь поля текстовые:
Description, Delivery Type и Phone.
И кнопка будет только одна - Submit Order.
А в правом блоке удали 2 кнопки снизу.

Сверху есть табличка с товарами. У нас эта табличка будет работать через ajax. Т.е. при нажатии на +/- будут добавляться или удаляться товары.
Сделай это - аналогично как ты делала работоспособность кнопочки Add To Cart.

Когда у нас теперь работают все кнопки - нужно сделать теперь обработку заказа.
В экшине - обработай заказ товаров. Помести все товары из корзины в таблицу ордеров и деталей ордеров и после этого - очисти корзину.
