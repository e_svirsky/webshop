<?php

namespace Framework\Http;

use App\Config\Configuration;
use Framework\Exceptions\NotFoundException;

class Routing
{
    const REGULAR_FOR_INCOME_URL = "/(\/){1}(\d+)/";
    const REGULAR_FOR_ROUTE_TABLE_URL = "/(\{){1}(\w+)(\}){1}/";

    public static $routeTable;

    public static $currentRouteName;

    function __construct()
    {
        self::$routeTable = Configuration::routeTable();
    }

    public function parse()
    {
        $postParams = array();

        $arr = explode("?", $_SERVER["REQUEST_URI"]);
        $firstRequestPart = $arr[0];
        $paramsParts = array();
        if (isset($arr[1]) && $arr[1]) {
            $paramsParts = explode("&", $arr[1]);
        }

        $getParams = array();
        foreach ($paramsParts as $key => $value) {
            $ps = explode("=", $value);
            $getParams[$ps[0]] = $ps[1];
        }

        $controllerName = null;
        $actionName = null;
        $result = preg_replace(self::REGULAR_FOR_INCOME_URL, "/{}", $firstRequestPart);

        foreach (self::$routeTable as $k => $route) {

            $routeReplaced = preg_replace(self::REGULAR_FOR_ROUTE_TABLE_URL, "{}", $route[0]);

            if ($result === $routeReplaced) {

                preg_match_all(self::REGULAR_FOR_INCOME_URL, $firstRequestPart, $aValues);
                preg_match_all(self::REGULAR_FOR_ROUTE_TABLE_URL, $route[0], $aKeys);

                $getParamsInUrl = array();
                if (count($aValues[0]) == count($aKeys[0])) {
                    foreach ($aKeys[0] as $key => $value) {
                        $getParamsInUrl[trim($aKeys[0][$key], "{}")] = trim($aValues[0][$key], "/");
                    }

                    $getParams = array_merge($getParamsInUrl, $getParams);
                }
                self::$currentRouteName = $k;
                $controllerName = $route[1];
                $actionName = $route[2];
            }
        }

        $request = new Request();

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $postParams = $_POST;
            $request->setIsPost(true);
        }

        $request->setGetParameters($getParams);
        $request->setPostParameters($postParams);

        if ($actionName == null && $controllerName == null) {
            throw new NotFoundException('Unknown page');
        }
        $controllerName = 'App\\Controller\\' . ucfirst($controllerName . 'Controller');
        $class = new $controllerName();

        return $class->{$actionName . 'Action'}($request);
    }

    public static function generateUrl($routeName, $routeParams = array())
    {
        if (isset(self::$routeTable[$routeName])) {
            $string = self::$routeTable[$routeName][0];
            $aSecondPart = array();
            $secondPart = "";
            foreach ($routeParams as $key => $routeParam) {
                if (strpos(self::$routeTable[$routeName][0], "{".$key."}") !== false) {
                    $string = str_replace("{".$key."}", $routeParam, $string);
                } else {
                    $aSecondPart[] = $key."=".$routeParam;
                }
            }

            if (count($aSecondPart)) {
                $secondPart = implode("&", $aSecondPart);
            }

            if ($secondPart) {
                $string .= "?".$secondPart;
            }

            if (strpos($string, "{")) {
                throw new \Exception('You have entered not enough params');
            } else {
                return $string;
            }
        } else {
            throw new \Exception('This route does not exist');
        }
    }
}
