<?php

namespace Framework\Database;

use \ReflectionClass;
use \ReflectionProperty;

abstract class Repository
{
    private $isNew;

    private $removed = false;

    /**
     * @return boolean
     */
    public function isRemoved()
    {
        return $this->removed;
    }

    /**
     * @param boolean $removed
     */
    public function setRemoved($removed)
    {
        $this->removed = $removed;
    }

    public function __construct($isNew = true)
    {
        $this->isNew = $isNew;
    }
    public static function find($id)
    {
        $class = get_called_class();
        $obj = new $class(false);
        $id = (int)$id;
        $db = DatabaseConnection::getInstance();
        $result = $db->query('SELECT * FROM '.$obj::$staticTableName.' WHERE id='.$id, \PDO::FETCH_ASSOC);
        $fetchResult = $result->fetchAll();
        if(isset($fetchResult[0])) {
            foreach ($fetchResult[0] as $k => $v) {
                $arr = explode('_', $k);
                $arr[0] = ucfirst($arr[0]);
                $arr[1] = ucfirst($arr[1]);
                $u = 'set' . $arr[0] . $arr[1];
                $obj->$u($v);
            }

            return $obj;
        } else {
            return null;
        }

    }



    public static function findOneBy($params)
    {
        $objects = self::findBy($params);
        $result = null;
        if (count($objects)) {
            $result = $objects[0];
        }

        return $result;
    }

    public static function findBy($params)
    {
        $class = get_called_class();
        $obj = new $class(false);
        $api = new ReflectionClass($class);
        $properties = array();
        foreach ($api->getProperties(ReflectionProperty::IS_PRIVATE) as $p) {
            $name = $p->getName();
            array_push($properties, self::camelCaseToUnderscore($name));
        }
        $myParams = array();
        $str = array();
        $lim = array();
        $wasLimit  = false;
        $wasOffset = false;
        $wasOrderAsc = false;
        $wasOrderDesc = false;
        foreach ($params as $k => $v) {
            if ($k === 'limit') {
                $wasLimit = true;
                $k1 = strtoupper($k);
                array_unshift($lim, $k1 . ' ' . $v);
            }
            if ($k === 'offset') {
                $wasOffset = true;
                $k1 = strtoupper($k);
                array_push($lim, $k1 . ' ' . $v);
            }
            if ($k === 'order_by_asc') {
                $wasOrderAsc = true;
                $order = 'ORDER BY '. $v.' ASC';
            }
            if ($k === 'order_by_desc') {
                $wasOrderDesc = true;
                $order = 'ORDER BY '. $v.' DESC';
            }
            if($wasOffset != true && $wasLimit != true) {
                array_push($myParams, $k);
                $n = self::camelCaseToUnderscore($k);
                array_push($str, $n . ' = \'' . $v . '\'');
            }

        }
        if ($wasOrderAsc && $wasOrderDesc) {
            throw new \Exception('Error. Invalid order parameter');
        }
        if($wasOffset == true && $wasLimit != true){
            throw new \Exception('Error. You did not enter LIMIT');
        }
        $prop = implode(' AND ', $str);
        $prop1 = implode(' ', $lim);
        $arr = array_intersect($properties, $myParams);
        if (count($arr) != count($myParams)) {
            throw new \Exception('Unknown params');
        }
        $arrObj = array();
        $db = DatabaseConnection::getInstance();
        if(count($str)) {
            $result = $db->query('SELECT * FROM ' . $obj::$staticTableName . ' WHERE ' . $prop . ' ' .$order.' '. $prop1, \PDO::FETCH_ASSOC);
        }  else {
            $result = $db->query('SELECT * FROM ' . $obj::$staticTableName.' '.$order.' '.$prop1, \PDO::FETCH_ASSOC);
        }
        $fetchResult = $result->fetchAll();
        if(count($fetchResult)) {
            for($i = 0; $i < count($fetchResult); $i++) {
                $obj = new $class(false);
                foreach($fetchResult[$i] as $k => $v) {
                        $arr = explode('_', $k);
                        $arr[0] = ucfirst($arr[0]);
                        $arr[1] = ucfirst($arr[1]);
                        $u = 'set' . $arr[0] . $arr[1];
                        $obj->$u($v);
                    }
                array_push($arrObj, $obj);
            }
        } else {
            return null;
        }

        return $arrObj;
    }

    public function save()
    {
        $api = new ReflectionClass($this);
        $property = array();
        $value = array();
        $str = array();
        foreach($api->getProperties(ReflectionProperty::IS_PRIVATE) as $p)
        {
            $name = $p->getName();
            $func = 'get'.ucfirst($name);
            $result = self::camelCaseToUnderscore($name);
            $val = $this->$func();
            array_push($property,  $result);
            array_push($value, '\''.$val.'\'');

            $mem = $result.' = '.'\''.$val.'\'';
            array_push($str, $mem);
        }
        $properties = implode(', ', $property);
        $values = implode(', ', $value);
        $string = implode(', ', $str);
        $db = DatabaseConnection::getInstance();
        if($this->isNew == true) {
            $sql = 'INSERT INTO '.$this::$staticTableName.' ('.$properties.') VALUES ('.$values.')';
        } else {
            $sql = 'UPDATE '.$this::$staticTableName.' SET '.$string.' WHERE id='.$this->getId();
        }

        if(isset($db)) {
            $db->query($sql);
            $id = $db->lastInsertId();
            if ($id) {
                $this->setId($id);
            }
        } else {
            return null;
        }
    }

    public function remove()
    {
        $db = DatabaseConnection::getInstance();
        if($this->removed == true) {
            throw new \Exception('Error. Object has deleted');
        } else {
            $sql = 'DELETE FROM '.$this::$staticTableName.' WHERE id='.$this->getId();
            $this->removed = true;
        }
        if(isset($db)) {
            return $db->query($sql);
        } else {
            return null;
        }
    }

    private static function camelCaseToUnderscore($string)
    {
        return strtolower(preg_replace('/([a-z])([A-Z])/', '$1_$2', $string));
    }
}