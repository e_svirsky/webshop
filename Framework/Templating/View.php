<?php

namespace Framework\Templating;

use App\Config\Configuration;

class View
{
    function renderView($layoutFile, $file, $vars=null) {
        ob_start();
        include $layoutFile;
        $layoutContent = ob_get_clean();

        if (is_array($vars) && !empty($vars)) {
            extract($vars);
        }
        ob_start();
        $rootDir = Configuration::getRootDir();
        include $rootDir.'App/View/'.$file;
        $content =  ob_get_clean();

        return str_replace('%content%', $content, $layoutContent);
    }
}
