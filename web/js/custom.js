$(function () {
    var updateTotals = function () {
        var sum = 0;
        $('.cart_page').find('.good_item').each(function() {
            $(this).find('.cart_total_price').text('$' + $(this).find('.cart_quantity_input').val()*$(this).find('.prise_item').text().substr(1));
            sum = sum + parseInt($(this).find('.cart_total_price').text().substr(1), 10);
        });
        $('.total_price_all_goods').text('$' + sum);
        var tax = parseInt($('.eco_tax').text().substr(1), 10);
        $('.total_price_tax_free').text('$' + (sum + tax));
        $('#total_sum').val(sum + tax);
    };
    updateTotals();
    $('.add-to-cart').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
        var data = {};
        if ($('#good_count').length) {
            data['count'] = $('#good_count').val();
        }
        $.ajax($(this).attr('href'), {type: 'POST', data: data})
            .success(function (result) {
                $('#modal1').modal('show');
            })
            .error(function () {
                $('#modal2').modal('show');
            });

    });

    $('.cart_quantity_up').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
        processCartButtons($(this));
    });
    $('.cart_quantity_down').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
        processCartButtons($(this));
    });
    $('.cart_quantity_delete').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
        processCartButtons($(this));
    });

    var processCartButtons = function (element) {
        var className = element.attr('class');
        var data = {};
        var inputValue = element.parents('tr').find('input').val();
        switch (className) {
            case 'cart_quantity_up':
                var operation = '+';
                break;
            case 'cart_quantity_down':
                operation = '-';
                break;
            case 'cart_quantity_delete':
                operation = 'del';
                if (inputValue == 0) {
                    element.parents('tr').remove();
                    return false;
                }
                break;
        }
        data['operation'] = operation;
        data['goodId'] = element.parents('tr').attr('data-id');

        $.ajax((element).parents('table').attr('data-url'), {type: 'POST', data: data})
            .success(function (response) {
                var result = JSON.parse(response);
                if (result['status'] === 1) {
                    switch (result['operation']) {
                        case '-':
                            inputValue--;
                            element.parents('tr').find('input').val(inputValue);
                            break;
                        case '+':
                            inputValue++;
                            element.parents('tr').find('input').val(inputValue);
                            break;
                        case 'del':
                            element.parents('tr').remove();
                            break;
                    }
                } else {
                    alert(result['message']);
                }
                updateTotals();
            });
    };

    $('#submit_order').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
        var data = {};
        if ($('.description1').val() && $('.delivery_type').val() && $('.phone').val()) {
            $('#order_form').submit();
        } else {
            if (!$('.description1').val()) {
                $('.description1').addClass('error');
            }
            if (!$('.delivery_type').val()) {
                $('.delivery_type').addClass('error');
            }
            if (!$('.phone').val()) {
                $('.phone').addClass('error');
            }

            $('#modal3').modal('show');
        }
    });
   $('.description1, .delivery_type, .phone').on('focus', function (e) {
       $(this).removeClass('error');
   });

    $('#submit_contact').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();

        if ($('#name_contact').val() && $('#email_contact').val() && $('#subject_contact').val() && $('#message').val()) {
            $('#main-contact-form').submit();

            return false;
        } else {
            if (!$('#name_contact').val()) {
                $('#name_contact').addClass('error');
            }
            if (!$('#email_contact').val()) {
                $('#email_contact').addClass('error');
            }
            if (!$('#subject_contact').val()) {
                $('#subject_contact').addClass('error');
            }
            if (!$('#message').val()) {
                $('#message').addClass('error');
            }

            $('#modal3').modal('show');
        }
    });

    $('#name_contact, #email_contact, #subject_contact, #message').on('focus', function (e) {
        $(this).removeClass('error');
    });





});